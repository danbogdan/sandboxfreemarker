import directives.ReplaceDirective;
import freemarker.template.*;
import javafx.beans.property.SimpleBooleanProperty;
import methods.CompareAgeOfUsers;
import objectWrapper.CustomObjectWrapper;
import valueObjects.User;

import java.io.File;
import java.io.FileWriter;
import java.io.Writer;
import java.sql.Date;
import java.util.*;

public class Main {

    public static void main(String[] args) throws Exception {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
        cfg.setDirectoryForTemplateLoading(new File("./templates"));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setLogTemplateExceptions(false);
        cfg.setWrapUncheckedExceptions(true);
        cfg.setFallbackOnNullLoopVariable(false);
        cfg.setObjectWrapper(new CustomObjectWrapper(Configuration.VERSION_2_3_29));

        Map<String, Object> root = new HashMap<>();
        ArrayList<User> users = new ArrayList<>();
        users.add(new User("James", "Jefferson", 30));
        users.add(new User("Jeff", "Jameson", 23));
        users.add(new User("Joe", "Biden", Integer.MAX_VALUE));
        root.put("users", users);

        //scalars
        root.put("text", new SimpleScalar("This is a example text to describe the web site"));
        root.put("text2", "Thats just a normal text in a normal string");
        root.put("scalars", new Object[]{new SimpleScalar("This is a simple Scalar"), new SimpleNumber(10),
                new SimpleBooleanProperty(false), new SimpleDate(new Date(1))});
        //containers
        SimpleHash hash = new SimpleHash();
        hash.put("example1", "data1");
        hash.put("example2", "data2");
        hash.put("example3", "data3");
        root.put("hash", hash);
        SimpleSequence sequence = new SimpleSequence();
        sequence.add("data1");
        sequence.add("data2");
        sequence.add("data3");
        root.put("sequence", sequence);
        SimpleCollection collection = new SimpleCollection(users.iterator());
        root.put("collection", collection);

        //methods
        root.put("compareAgeOfUsers", new CompareAgeOfUsers());

        //directives
        root.put("replace", new ReplaceDirective());

        Template template = cfg.getTemplate("index.ftl");
        try (Writer fileWriter = new FileWriter("output.html")) {
            template.process(root, fileWriter);
        }
    }
}
