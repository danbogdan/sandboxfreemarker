package objectWrapper;

import freemarker.template.*;
import valueObjects.User;

public class UserAdapter extends WrappingTemplateModel implements TemplateSequenceModel, AdapterTemplateModel {

    private final User user;

    public UserAdapter(ObjectWrapper objectWrapper, User user) {
        super(objectWrapper);
        this.user = user;
    }

    @Override
    public Object getAdaptedObject(Class<?> aClass) {
        return user;
    }

    @Override
    public TemplateModel get(int i) throws TemplateModelException {
        switch (i){
            case 0:
                return new SimpleScalar(user.getFirstname());
            case 1:
                return new SimpleScalar(user.getLastname());
            case 2:
                return new SimpleNumber(user.getAge());
            default:
                throw new TemplateModelException("Wrong Index");
        }
    }

    @Override
    public int size() throws TemplateModelException {
        return 3;
    }
}
