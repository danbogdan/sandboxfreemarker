package objectWrapper;

import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;
import valueObjects.User;

public class CustomObjectWrapper extends DefaultObjectWrapper {

    public CustomObjectWrapper(Version incompatibleImprovements) {
        super(incompatibleImprovements);
    }

    @Override
    protected TemplateModel handleUnknownType(final Object obj) throws TemplateModelException {
        if(obj instanceof User){
            return new UserAdapter(this, (User)obj);
        }
        return super.handleUnknownType(obj);
    }
}
