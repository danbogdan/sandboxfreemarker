package methods;

import freemarker.template.SimpleNumber;
import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import valueObjects.User;

import java.util.List;

public class CompareAgeOfUsers implements TemplateMethodModelEx {
    @Override
    public Object exec(List list) throws TemplateModelException {
        if(list.size() != 4){
            throw new TemplateModelException("Wrong arguments");
        }
        SimpleNumber user1age = (SimpleNumber) list.get(0), user2age = (SimpleNumber) list.get(2);
        if(user1age.getAsNumber().intValue() > user2age.getAsNumber().intValue()){
            return new SimpleScalar(list.get(1) + " is older");
        }else {
            return new SimpleScalar(list.get(3) + " is older");
        }
    }
}
