package directives;

import freemarker.core.Environment;
import freemarker.template.*;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class ReplaceDirective implements TemplateDirectiveModel {

    @Override
    public void execute(Environment environment, Map params, TemplateModel[] templateModels, TemplateDirectiveBody templateDirectiveBody) throws TemplateException, IOException {
        if(params.size() != 2){
            throw new TemplateModelException("Wrong parameter.");
        }
        if(templateModels.length != 0){
            throw new TemplateModelException("This directive doesn't allow loop variables.");
        }
        if(templateDirectiveBody != null){
            templateDirectiveBody.render(new ReplaceWriter(environment.getOut(), params.get("oldChar").toString().charAt(0), params.get("newChar").toString().charAt(0)));
        }else {
            throw new RuntimeException("Missing body");
        }
    }

    private static class ReplaceWriter extends Writer {

        private final Writer out;
        private char oldChar;
        private char newChar;

        ReplaceWriter (Writer out, char oldChar, char newChar) {
            this.out = out;
            this.oldChar = oldChar;
            this.newChar = newChar;
        }

        public void write(char[] cbuf, int off, int len) throws IOException {
            char[] transformedCbuf = new char[len];
            boolean argActive = false;
            for (int i = 0; i < len; i++) {
                if(cbuf[i] == '$' && !argActive){
                    argActive = true;
                    continue;
                }
                if(cbuf[i] != oldChar){
                    transformedCbuf[i] = cbuf[i];
                }else {
                    if(argActive){
                        argActive = false;
                        transformedCbuf[i] = cbuf[i];
                    }else {
                        transformedCbuf[i] = newChar;
                    }
                }
            }
            out.write(transformedCbuf);
        }

        public void flush() throws IOException {
            out.flush();
        }

        public void close() throws IOException {
            out.close();
        }
    }
}
